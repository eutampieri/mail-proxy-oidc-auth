use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
};
use tokio_rustls::rustls::ClientConfig;

use crate::get_from_env_or_panic;

pub async fn handle(
    in_conn: tokio_rustls::server::TlsStream<TcpStream>,
    config: ClientConfig,
    server_name: tokio_rustls::rustls::pki_types::ServerName<'static>,
) {
    let conn = tokio_rustls::TlsConnector::from(std::sync::Arc::new(config));

    let mut sock = TcpStream::connect(&format!(
        "{}:{}",
        get_from_env_or_panic("IMAP_SERVER"),
        get_from_env_or_panic("IMAP_PORT")
    ))
    .await
    .unwrap();
    let tls = conn.connect(server_name, &mut sock).await.unwrap();

    let (mut client_recv, mut client_send) = tokio::io::split(in_conn);
    let (mut server_recv, mut server_send) = tokio::io::split(tls);

    let handle_one = async { tokio::io::copy(&mut server_recv, &mut client_send).await };
    let handle_two = async {
        let mut authenticated = false;
        let mut authenticate_label;
        let mut waiting_for_creds = false;
        let mut buffer = [0; 1000];
        while !authenticated {
            let read = client_recv.read(&mut buffer).await.unwrap();
            let command = String::from_utf8(buffer[0..read].to_vec()).unwrap();
            let is_authenticate_command = command.to_lowercase().contains("authenticate");
            if is_authenticate_command || waiting_for_creds {
                waiting_for_creds = false;
                let format_str_is_full;
                authenticate_label = Some(command.split(' ').next().unwrap().to_string());
                let creds = if let Some(creds) = &command[0..command.len() - 2].split(' ').nth(3) {
                    format_str_is_full = true;
                    creds
                } else {
                    format_str_is_full = false;
                    waiting_for_creds = is_authenticate_command;
                    &command[0..command.len() - 2]
                };
                if waiting_for_creds {
                    server_send.write_all(&buffer[0..read]).await.unwrap();
                    continue;
                }
                let creds = crate::login::parse_creds(creds);
                if crate::login::login(&creds.0, &creds.1).await {
                    let creds = crate::login::format_creds(
                        &get_from_env_or_panic("IMAP_USERNAME"),
                        &get_from_env_or_panic("IMAP_PASSWORD"),
                    );
                    server_send
                        .write_all(
                            if format_str_is_full {
                                format!(
                                    "{} AUTHENTICATE PLAIN {}\r\n",
                                    authenticate_label.unwrap(),
                                    creds
                                )
                            } else {
                                format!("{}\r\n", creds)
                            }
                            .as_bytes(),
                        )
                        .await
                        .unwrap();
                    authenticated = true;
                    waiting_for_creds = false;
                } else {
                    server_send
                        .write_all(
                            format!("{}\r\n", crate::login::format_creds("invalid", "invalid"))
                                .as_bytes(),
                        )
                        .await
                        .unwrap();
                }

                continue;
            }
            server_send.write_all(&buffer[0..read]).await.unwrap();
        }

        tokio::io::copy(&mut client_recv, &mut server_send).await
    };
    tokio::try_join!(handle_one, handle_two).unwrap();
}
