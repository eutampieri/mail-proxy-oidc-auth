use rustls::RootCertStore;
use std::sync::Arc;
use tokio_rustls::rustls;

mod imap;
mod login;
mod smtp;

#[inline(always)]
fn get_from_env_or_panic(var: &'static str) -> String {
    std::env::var(var).expect(&format!(
        "Missing {} environment variable. Please provide it",
        var
    ))
}

#[tokio::main]
async fn main() {
    let root_store = RootCertStore {
        roots: webpki_roots::TLS_SERVER_ROOTS.into(),
    };
    let mut client_config = rustls::ClientConfig::builder()
        .with_root_certificates(root_store)
        .with_no_client_auth();
    // Allow using SSLKEYLOGFILE.
    client_config.key_log = Arc::new(rustls::KeyLogFile::new());

    let server_name: rustls::pki_types::ServerName<'_> =
        get_from_env_or_panic("IMAP_SERVER").try_into().unwrap();

    let cert_file = get_from_env_or_panic("CERT_FILE");
    let private_key_file = get_from_env_or_panic("PRIVKEY_FILE");

    let certs = rustls_pemfile::certs(&mut std::io::BufReader::new(
        &mut std::fs::File::open(cert_file).unwrap(),
    ))
    .collect::<Result<Vec<_>, _>>()
    .unwrap();
    let private_key = rustls_pemfile::private_key(&mut std::io::BufReader::new(
        &mut std::fs::File::open(&private_key_file).unwrap(),
    ))
    .unwrap()
    .unwrap();
    let imap_server_config = Arc::new(
        rustls::ServerConfig::builder()
            .with_no_client_auth()
            .with_single_cert(certs.clone(), private_key)
            .unwrap(),
    );

    let private_key = rustls_pemfile::private_key(&mut std::io::BufReader::new(
        &mut std::fs::File::open(private_key_file).unwrap(),
    ))
    .unwrap()
    .unwrap();

    let smtp_server_config = Arc::new(
        rustls::ServerConfig::builder()
            .with_no_client_auth()
            .with_single_cert(certs, private_key)
            .unwrap(),
    );

    let imap = {
        let imap_listener =
            tokio::net::TcpListener::bind(get_from_env_or_panic("IMAP_BIND_ADDRESS"))
                .await
                .unwrap();
        tokio::spawn(async move {
            while let Ok((in_conn, _)) = imap_listener.accept().await {
                let acceptor = tokio_rustls::TlsAcceptor::from(imap_server_config.clone());
                if let Ok(in_conn) = acceptor.accept(in_conn).await {
                    //conn.complete_io(&mut in_conn).unwrap();

                    let config = client_config.clone();

                    let server_name = server_name.clone();
                    tokio::spawn(async move { imap::handle(in_conn, config, server_name).await });
                } else {
                    eprintln!("Failed to accept TLS connection (IMAP)");
                }
            }
        })
    };
    let smtp = {
        let smtp_listener =
            tokio::net::TcpListener::bind(get_from_env_or_panic("SMTP_BIND_ADDRESS"))
                .await
                .unwrap();
        tokio::spawn(async move {
            while let Ok((in_conn, _)) = smtp_listener.accept().await {
                let acceptor = tokio_rustls::TlsAcceptor::from(smtp_server_config.clone());
                if let Ok(in_conn) = acceptor.accept(in_conn).await {
                    tokio::spawn(async move { smtp::handle(in_conn).await });
                } else {
                    eprintln!("Failed to accept TLS connection (SMTP)");
                }
            }
        })
    };

    tokio::try_join!(imap, smtp).unwrap();
}
