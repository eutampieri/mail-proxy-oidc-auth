use base64::Engine;
use hreq::prelude::*;

use crate::get_from_env_or_panic;

pub fn parse_creds(creds: &str) -> (String, String) {
    let (username, password) =
        if let Some(x) = base64::engine::general_purpose::STANDARD.decode(creds).ok() {
            let username_start = x.iter().position(|x| *x == 0).unwrap();
            let password_start = username_start
                + x.iter()
                    .skip(username_start + 2)
                    .position(|x| *x == 0)
                    .unwrap();
            let username =
                String::from_utf8(x[(username_start + 1)..(password_start + 2)].to_vec()).unwrap();
            let password = String::from_utf8(x[(password_start + 3)..].to_vec()).unwrap();
            (username, password)
        } else {
            let mut pieces = creds.split(" ").skip(2);
            (
                pieces.next().unwrap().to_owned(),
                pieces.next().unwrap().to_owned(),
            )
        };

    (username, password)
}

pub fn format_creds(username: &str, password: &str) -> String {
    let mut raw = vec![0];
    raw.append(&mut username.as_bytes().to_vec());
    raw.push(0);
    raw.append(&mut password.as_bytes().to_vec());
    base64::engine::general_purpose::STANDARD.encode(raw)
}

pub async fn login(username: &str, password: &str) -> bool {
    let body = form_urlencoded::Serializer::new(String::new())
        .append_pair("client_id", &get_from_env_or_panic("OIDC_CLIENT_ID"))
        .append_pair(
            "client_secret",
            &get_from_env_or_panic("OIDC_CLIENT_SECRET"),
        )
        .append_pair("username", username)
        .append_pair("password", password)
        .append_pair("grant_type", "password")
        .finish();
    let response = Request::builder()
        .uri(&get_from_env_or_panic("OIDC_TOKEN_ENDPOINT"))
        .method("POST")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .send(body)
        .await
        .unwrap();
    let status: u16 = response.status_code();
    if status != 200 {
        false
    } else {
        let response = response
            .into_body()
            .read_to_json::<serde_json::Value>()
            .await
            .unwrap();

        let jwt = response
            .as_object()
            .unwrap()
            .get("access_token")
            .unwrap()
            .as_str()
            .unwrap();
        let claims = jwt.split('.').nth(1).unwrap();
        let claims = base64::engine::general_purpose::URL_SAFE_NO_PAD
            .decode(claims)
            .unwrap();
        let claims = serde_json::from_slice::<serde_json::Value>(&claims).unwrap();
        let roles = claims
            .as_object()
            .unwrap()
            .get("realm_access")
            .unwrap()
            .as_object()
            .unwrap()
            .get("roles")
            .unwrap()
            .as_array()
            .unwrap()
            .iter()
            .map(|x| x.as_str().unwrap().to_string())
            .collect::<std::collections::HashSet<_>>();
        roles.contains(&get_from_env_or_panic("OIDC_USER_ROLE"))
    }
}
