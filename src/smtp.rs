use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
};

use crate::get_from_env_or_panic;

pub async fn handle(in_conn: tokio_rustls::server::TlsStream<TcpStream>) {
    let allowed_commands = {
        let mut hs = std::collections::HashSet::new();
        hs.insert("ehlo");
        hs.insert("helo");
        hs.insert("noop");
        hs.insert("rset");
        hs.insert("quit");
        hs
    };

    let mut sock = TcpStream::connect(&format!(
        "{}:{}",
        get_from_env_or_panic("SMTP_SERVER"),
        get_from_env_or_panic("SMTP_PORT")
    ))
    .await
    .unwrap();

    let (mut client_recv, mut client_send) = tokio::io::split(in_conn);
    let (mut server_recv, mut server_send) = sock.split();
    let (mpsc_write, mut mpsc_read) = tokio::sync::mpsc::channel(10);
    let client_write = mpsc_write.clone();

    let server_reader = async move {
        let mut buffer = [0; 1000];
        let mut auth_sent = false;
        while let Ok(read) = server_recv.read(&mut buffer).await {
            let mut to_send = buffer[0..read].to_vec();
            if !auth_sent {
                let message = String::from_utf8(buffer[0..read].to_vec()).unwrap();
                if message.contains("250-PIPELINING") {
                    if !message.contains("250-AUTH") {
                        let mut pieces = message.split("\r\n").collect::<Vec<_>>();
                        pieces.pop().unwrap();
                        let last = pieces.pop().unwrap();
                        pieces.push("250-AUTH LOGIN PLAIN");
                        pieces.push(last);
                        pieces.push("");
                        to_send = pieces.join("\r\n").as_bytes().to_vec();
                    }
                    auth_sent = true;
                }
            }
            mpsc_write.send(to_send).await.unwrap();
        }
    };
    let client_writer = async move {
        while let Some(data) = mpsc_read.recv().await {
            client_send.write_all(&data).await.unwrap();
        }
    };
    let handle_two = async move {
        let mut authenticated = false;
        let mut buffer = [0; 1000];
        while !authenticated {
            let read = client_recv.read(&mut buffer).await.unwrap();
            let command = String::from_utf8(buffer[0..read].to_vec()).unwrap();
            let command_name = command.split(' ').next().unwrap().to_lowercase();
            if command_name == "auth" {
                let creds = command.split(' ').nth(2).unwrap();
                let creds = crate::login::parse_creds(&creds[0..creds.len() - 2]);
                if crate::login::login(&creds.0, &creds.1).await {
                    authenticated = true;
                    client_write
                        .send(b"235 2.7.0 Authentication successful\r\n".to_vec())
                        .await
                        .unwrap()
                } else {
                    client_write
                        .send(b"5.7.8  Authentication credentials invalid\r\n".to_vec())
                        .await
                        .unwrap();
                }
            } else if !allowed_commands.contains(command_name.as_str()) {
                client_write
                    .send(b"530 5.7.0  Authentication required\r\n".to_vec())
                    .await
                    .unwrap();
            } else {
                server_send.write_all(&buffer[0..read]).await.unwrap();
            }
        }

        tokio::io::copy(&mut client_recv, &mut server_send)
            .await
            .unwrap();
    };
    tokio::join!(server_reader, client_writer, handle_two);
}
